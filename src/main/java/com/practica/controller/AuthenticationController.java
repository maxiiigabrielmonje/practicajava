package com.practica.controller;

import com.practica.dto.AuthenticationResponseDto;
import com.practica.dto.AutheticationRequestDto;
import com.practica.serviceImp.AuthenticationServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class AuthenticationController {

    @Autowired
    private AuthenticationServiceImp authenticationServiceImp;

    @PreAuthorize("permitAll")
    @PostMapping("/login")
    public ResponseEntity<AuthenticationResponseDto> login(@RequestBody AutheticationRequestDto authRequest){

        AuthenticationResponseDto jwtDto = authenticationServiceImp.login(authRequest);

        return ResponseEntity.ok(jwtDto);
    }
}
