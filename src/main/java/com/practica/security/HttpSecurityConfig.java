package com.practica.security;

import com.practica.security.filter.JwtAuthenticationFilter;
import com.practica.util.Permission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AuthorizeHttpRequestsConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Component;

@Component
@EnableWebSecurity
@EnableMethodSecurity()
public class HttpSecurityConfig {

    @Autowired
    private AuthenticationProvider authenticationProvider;

    @Autowired
    private JwtAuthenticationFilter jwtAuthenticationFilter;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {

        http
                .csrf().disable()
                .cors().disable()
                .sessionManagement(sessionMangConfig -> sessionMangConfig.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .authenticationProvider(authenticationProvider)
                .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
                .authorizeHttpRequests(builderRequestMatchers());


        return http.build();
    }

    private Customizer<AuthorizeHttpRequestsConfigurer<HttpSecurity>.AuthorizationManagerRequestMatcherRegistry> builderRequestMatchers() {
        return authConfig -> {

            //TODOS
            authConfig.requestMatchers(HttpMethod.POST,"/auth/login").permitAll();
            authConfig.requestMatchers(HttpMethod.POST, "/api/users").permitAll();
            authConfig.requestMatchers(HttpMethod.PUT, "/api/users/{id}").permitAll();
            authConfig.requestMatchers("/error").permitAll();

            //SOLO ADMIN POR AHORA
            authConfig.requestMatchers(HttpMethod.DELETE, "/api/users/{id}").hasAnyAuthority(Permission.DELETE_USERS.name(), Permission.ALL_PERMISSIONS.name());
            authConfig.requestMatchers(HttpMethod.GET,"/api/users").hasAnyAuthority(Permission.READ_ALL_USERS.name(), Permission.ALL_PERMISSIONS.name());

            authConfig.anyRequest().denyAll();
        };
        /*return authConfig -> {
            // Permitir todas las solicitudes
            authConfig.anyRequest().permitAll();
        };*/
    }
}
