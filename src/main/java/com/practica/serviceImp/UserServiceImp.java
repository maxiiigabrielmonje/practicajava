package com.practica.serviceImp;

import com.practica.model.User;
import com.practica.repository.UserRepository;
import com.practica.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class UserServiceImp implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public User save(User user) {
        validateUser(user);

        
       String hashedPassword = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt());

        user.setPassword(hashedPassword);

        user.setActive(true);
        return userRepository.save(user);
    }

    @Override
    public User findById(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new RuntimeException("El usuario no existe"));
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    @Transactional
    public User update(Long id, User user) {
        validateUser(user);
        User existingUser = findById(id);
        existingUser.setUsername(user.getUsername());
        existingUser.setPassword(user.getPassword());
        existingUser.setEmail(user.getEmail());
        existingUser.setName(user.getName());
        existingUser.setSurname(user.getSurname());
        existingUser.setActive(user.getActive());
        return userRepository.save(existingUser);
    }

    private void validateUser(User user) {
        if (StringUtils.isEmpty(user.getUsername())) {
            throw new RuntimeException("El nombre de usuario es obligatorio");
        }
        if (StringUtils.isEmpty(user.getPassword())) {
            throw new RuntimeException("La contraseña es obligatoria");
        }
        if (StringUtils.isEmpty(user.getEmail())) {
            throw new RuntimeException("El correo electrónico es obligatorio");
        }
        if (userRepository.existsByUsername(user.getUsername())) {
            throw new RuntimeException("El nombre de usuario ya existe");
        }
        if (userRepository.existsByEmail(user.getEmail())) {
            throw new RuntimeException("El correo electrónico ya existe");
        }
    }
}
