package com.practica.serviceImp;

import com.practica.dto.AuthenticationResponseDto;
import com.practica.dto.AutheticationRequestDto;
import com.practica.model.User;
import com.practica.repository.UserRepository;
import com.practica.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class AuthenticationServiceImp implements AuthenticationService {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtServiceImp jwtServiceImp;

    @Override
    public AuthenticationResponseDto login(AutheticationRequestDto authRequest) {

        /*
        Aca compara el username y la password
        Tambien hace todo el proceso que necesita la password
        para decodear, etc
        */
        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
            authRequest.getUsername(), authRequest.getPassword()
        );

        authenticationManager.authenticate(authToken);

        User user  = userRepository.findByUsername(authRequest.getUsername())
                .orElseThrow(()-> new RuntimeException("User not fouund")) ;

        String jwt = jwtServiceImp.generateToken(user, generateExtraClaims(user));

        AuthenticationResponseDto jwtDto = new AuthenticationResponseDto(jwt);

        return jwtDto;

    }

    @Override
    public Map<String, Object> generateExtraClaims(User user){

        Map<String, Object> extraClaims = new HashMap<>();

        extraClaims.put("name", user.getName());
        extraClaims.put("role", user.getRole().name());
        extraClaims.put("permissions", user.getAuthorities());

        return extraClaims;

    }
}
