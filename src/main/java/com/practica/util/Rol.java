package com.practica.util;


import lombok.Getter;


import java.util.Arrays;
import java.util.List;

@Getter
public enum Rol {

    USER_REGISTER(Arrays.asList(Permission.CREATE_USERS,Permission.READ_USER, Permission.UPDATE_USERS)),

    ADMINISTRATOR(Arrays.asList(Permission.ALL_PERMISSIONS));

    private List<Permission> permissions;

    Rol(List<Permission> permissions) {
        this.permissions = permissions;
    }


}
