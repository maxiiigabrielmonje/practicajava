package com.practica.util;

public enum Permission {

    DELETE_USERS,
    CREATE_USERS,
    UPDATE_USERS,
    READ_USER,
    READ_ALL_USERS,
    ALL_PERMISSIONS
}
