package com.practica.service;

import com.practica.dto.AuthenticationResponseDto;
import com.practica.dto.AutheticationRequestDto;
import com.practica.model.User;

import java.util.Map;

public interface AuthenticationService {

    AuthenticationResponseDto login(AutheticationRequestDto authRequest);

    Map<String, Object> generateExtraClaims(User user);
}
