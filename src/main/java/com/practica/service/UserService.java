package com.practica.service;

import com.practica.model.User;
import java.util.List;

public interface UserService {

    User save(User user);

    User findById(Long id);

    List<User> findAll();

    void deleteById(Long id);

    User update(Long id, User user);
}
