package com.practica.service;

import com.practica.model.User;

import java.util.Map;

public interface JwtService {

    String generateToken(User user, Map<String, Object> extraClaims);

    String extractUsername(String jwt);
}
